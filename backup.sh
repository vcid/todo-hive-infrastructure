#!/bin/bash

# Set the paths of the directories you want to upload
dir1="/root/todo-hive-infrastructure/db"
dir2="/root/todo-hive-infrastructure/flask_app"

# Set the names of the S3 buckets you want to upload to
bucket1="todo-hive-backup"


# Upload directories to S3
aws s3 sync "$dir1" "s3://$bucket1/db"
aws s3 sync "$dir2" "s3://$bucket1/flask_app"
