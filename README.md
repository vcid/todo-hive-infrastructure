# ToDo Hive - Infrastructure
Aufbau der Infrastruktur für die Webapplikation ToDo-Hive welche mit Flask entwickelt wurde.

ToDo Hive - Application Repository:
https://gitlab.com/vcid/todo-hive


## About

In diesem Repository wird die Infrastruktur aufgebaut um eine Web Applikation auf einer AWS EC2-Instanz zu betreiben.
Die EC2-Instanz muss vorher installiert werden, damit per SSH darauf zugegriffen werden kann.

## Docker Services
Dieses Projekt verwendet Docker Compose, um die folgenden Services zu erstellen:

Nginx: Als Reverse Proxy für die Flask-Anwendung und zum Bereitstellen von SSL/TLS.
MySQL: Als Datenbank-Backend für die Anwendung.
Flask App: Die ToDo Hive-Anwendung selbst, die auf einem Python-Flask-Server läuft.
Die Konfiguration dieser Services finden Sie in der docker-compose.yml Datei.

## CI/CD
Das Projekt verwendet GitLab CI/CD, um den Deploy-Prozess zu automatisieren. Die Pipeline besteht aus einer einzigen Stage namens "deploy" und wird durch die .gitlab-ci.yaml Datei konfiguriert. Der Prozess umfasst das Herunterfahren
